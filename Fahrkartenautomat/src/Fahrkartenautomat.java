import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		String jaNein = "Ja";

		do {

			double ticket = fahrkartenbestellungErfassen();

			double r�ckgabebetrag = fahrkartenBezahlen(ticket);

			fahrkartenAusgeben();

			rueckgeldAusgeben(r�ckgabebetrag);

			System.out.println("");

			System.out.println("M�chten Sie eine weitere Karte kaufen? Ja oder Nein?");

			Scanner myScanner = new Scanner(System.in);

			jaNein = myScanner.nextLine();

			System.out.println("");

		} while (jaNein.equals("Ja"));

	}

	public static double fahrkartenbestellungErfassen() {

		String[] fahrkarten = new String[10];

		fahrkarten[0] = "1. Einzelfahrschein Berlin AB";
		fahrkarten[1] = "2. Einzelfahrschein Berlin BC";
		fahrkarten[2] = "3. Einzelfahrschein Berlin ABC";
		fahrkarten[3] = "4. Kurzstrecke";
		fahrkarten[4] = "5. Tageskarte Berlin AB";
		fahrkarten[5] = "6. Tageskarte Berlin BC";
		fahrkarten[6] = "7. Tageskarte Berlin ABC";
		fahrkarten[7] = "8. Kleingruppen-Tageskarte Berlin AB";
		fahrkarten[8] = "9. Kleingruppen-Tageskarte Berlin BC";
		fahrkarten[9] = "10 Kleingruppen-Tageskarte Berlin ABC";

		float[] fahrkartenpreis = new float[10];

		fahrkartenpreis[0] = 2.90f;
		fahrkartenpreis[1] = 3.30f;
		fahrkartenpreis[2] = 3.60f;
		fahrkartenpreis[3] = 1.90f;
		fahrkartenpreis[4] = 8.90f;
		fahrkartenpreis[5] = 9.00f;
		fahrkartenpreis[6] = 9.60f;
		fahrkartenpreis[7] = 23.50f;
		fahrkartenpreis[8] = 24.30f;
		fahrkartenpreis[9] = 24.90f;

		for (int i = 0; i < 10; i++) {

			System.out.print(fahrkarten[i] + "  -  ");
			System.out.print(fahrkartenpreis[i] + "�");
			System.out.println("");

		}

		Scanner tastatur = new Scanner(System.in);

		double ticket = 0;

		int anzahlTickets = 0;

		System.out.println("");
		System.out.print("Welches Ticket m�chten Sie haben? (1-10)");
		System.out.println("");

		ticket = tastatur.nextInt();

		if (ticket < 1 || ticket > 10) {

			System.out.println("Eingabe ung�ltig");
			System.exit(0);

		}

		System.out.print("Wie viele Tickets m�chten sie kaufen? ");

		anzahlTickets = tastatur.nextInt();

		if (anzahlTickets > 10) {

			System.out.println("Sie k�nnen nicht mehr als 10 Tickets kaufen. Es wird jetzt ein Ticket ausgegeben");
			anzahlTickets = 1;

		}

		if (ticket == 1) {
			ticket = (2.90 * anzahlTickets);
		} else if (ticket == 2) {
			ticket = (3.30 * anzahlTickets);
		} else if (ticket == 3) {
			ticket = (3.60 * anzahlTickets);
		} else if (ticket == 4) {
			ticket = (1.90 * anzahlTickets);
		} else if (ticket == 5) {
			ticket = (8.60 * anzahlTickets);
		} else if (ticket == 6) {
			ticket = (9.00 * anzahlTickets);
		} else if (ticket == 7) {
			ticket = (9.60 * anzahlTickets);
		} else if (ticket == 8) {
			ticket = (23.50 * anzahlTickets);
		} else if (ticket == 9) {
			ticket = (24.30 * anzahlTickets);
		} else if (ticket == 10) {
			ticket = (24.90 * anzahlTickets);
		}

		return ticket;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag = 0.00;

		System.out.println();
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		System.out.println("");

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			Scanner tastatur = new Scanner(System.in);

			System.out.printf("Noch zu zahlen: %.2f Euro", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.println("");

			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		return r�ckgabebetrag;

	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");

		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {

		if (r�ckgabebetrag > 0.00)

			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ", r�ckgabebetrag);
		System.out.println("wird in folgenden M�nzen ausgezahlt:");

		while (r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
		{
			System.out.println("2 EURO");
			r�ckgabebetrag -= 2.00;
		}
		while (r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
		{
			System.out.println("1 EURO");
			r�ckgabebetrag -= 1.00;
		}
		while (r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
		{
			System.out.println("50 CENT");
			r�ckgabebetrag -= 0.50;
		}
		while (r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
		{
			System.out.println("20 CENT");
			r�ckgabebetrag -= 0.20;
		}
		while (r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
		{
			System.out.println("10 CENT");
			r�ckgabebetrag -= 0.10;
		}
		while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		{
			System.out.println("5 CENT");
			r�ckgabebetrag -= 0.05;
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}

}