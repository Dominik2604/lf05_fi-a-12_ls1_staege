
import java.util.ArrayList;

/**
 * @author dstaege
 *
 */

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> BroadcastKommunikatorn = new ArrayList<String>();
	private ArrayList<Ladung> Ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {

	}
	
	/**
	 * @return
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	/**
	 * @return
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	/**
	 * @return
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	/**
	 * @return
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**
	 * @return
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	/**
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	/**
	 * @return
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * @return
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * @return
	 */
	public ArrayList<String> getBroadcastKommunikatorn() {
		return BroadcastKommunikatorn;
	}

	/**
	 * @param broadcastKommunikatorn
	 */
	public void setBroadcastKommunikatorn(ArrayList<String> broadcastKommunikatorn) {
		BroadcastKommunikatorn = broadcastKommunikatorn;
	}

	/**
	 * @return
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return Ladungsverzeichnis;
	}

	/**
	 * @param ladungsverzeichnis
	 */
	public void setLadungsverzeichnis(ArrayList<String> ladungsverzeichnis) {
		Ladungsverzeichnis = Ladungsverzeichnis;
	}
	
}


