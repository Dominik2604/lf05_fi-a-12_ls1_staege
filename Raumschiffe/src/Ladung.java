/**
 * @author dstaege
 *
 */
public class Ladung {

	private String bezeichnung;
	private int menge;

	/**
	 * 
	 */
	public Ladung() {
		this.bezeichnung = "";
		this.menge = 0;
	}

	/**
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 *
	 */
	public String toString() {
		return "Bezeichnung: " + bezeichnung + ", Menge: " + menge;
	}

	/**
	 * @return
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * @return
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

}
