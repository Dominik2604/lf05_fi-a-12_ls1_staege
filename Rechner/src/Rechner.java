import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    float zahl1 = myScanner.nextFloat();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    float zahl2 = myScanner.nextFloat();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    float summe = zahl1 + zahl2;  
    float differenz = zahl1 - zahl2;  
    float produkt = zahl1 * zahl2;  
    float quotient = zahl1 / zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + summe);   
    
    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + differenz);   
    
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl1 + " * " + zahl2 + " = " + produkt);   
    
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    System.out.print(zahl1 + " / " + zahl2 + " = " + quotient);   
 
    myScanner.close(); 
     
  }    
}