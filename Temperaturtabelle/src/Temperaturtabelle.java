
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf( "Fahrenheit  | %10s\n" , "Celsius" );
		
		 char s = '-';
		    
		    for(int i=0;i<25;i++){
	            System.out.print(s); }
		
		System.out.println("");
		
		System.out.printf( "-20         |%11.6s\n", -28.8889);
	    System.out.printf( "-10         |%11.6s\n", -23.3333);
	    System.out.printf( "+0          |%11.6s\n", -17.7778);
	    System.out.printf( "+20         |%11.5s\n", -6.6667);
	    System.out.printf( "+30         |%11.5s\n", -1.1111);
	    
	}

}
